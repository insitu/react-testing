import React, { Component } from 'react';
// Redux store
import store from '../../store'
// Images
import image from './twitterLogo.svg';
// Styles
import './TwitterRefresh.css';
import '../../styles/flexboxgrid.min.css';

const superagent = require('superagent');

class TwitterLogin extends Component {

  constructor(props){
    super(props)

    // init properties
    this.state = {
      profile: {}
    }
    // init subscription to store
    store.subscribe( () => {
      this.setState({
        profile: store.getState().profile
      })
    })
  }

  componentDidMount() {
    //
  }

  render() {
    return (

      <div>

        <div className="row center-xs start-sm middle-xs">

          <div className="col-lg-3 col-lg-offset-3 col-md-4 col-md-offset-2 col-sm-5 col-sm-offset-1 col-xs-11">
            <div className="row">

              <div className="col-lg-3 col-md-2 col-sm-3 col-xs-4">
                <div className="twitter-logo">
                  <img src={image} alt="twitter"/>
                </div>
              </div>

              <div className="col-lg-9 col-md-10 col-xs-8">
                <div className="row start-xs">
                  <div className="col-xs-12 name">
                    {this.state.profile.name}
                  </div>
                  <div className="col-xs-12 account">
                    {this.state.profile.screen_name}
                  </div>
                  <div className="col-xs-12 email">
                    {this.state.profile.email}
                  </div>
                </div>
              </div>

            </div>
          </div>

          <div className="col-lg-3 col-md-4 col-sm-5 col-xs-11">
            <div className="row">
              <div className="col-xs-12 bio">
                {this.state.profile.description}
              </div>
            </div>
          </div>

        </div>

        <div className="row center-xs middle-xs">
          <div className="col-lg-2 col-md-3 col-sm-4 col-xs-5 twitter-button" onClick={handleClick}>
              Refresh
          </div>

          <div className="col-xs-1"></div>

          <div className="col-lg-2 col-md-3 col-sm-4 col-xs-5 twitter-button" onClick={handleLogout}>
              Logout
          </div>

        </div>

      </div>

    );
  }

}

function handleClick(e) {
  e.preventDefault();


  superagent
    .get(window.urlAPI + '/tweets')
    .withCredentials()
    .end((err, res) => {

      console.log(res);
      console.log(err);
      // Calling the end function will send the request
    });
}


function handleLogout(e) {
  e.preventDefault();


  superagent
    .post(window.urlAPI + '/disconnect')
    .withCredentials()
    .end((err, res) => {

      if (err) {
        alert(err)
        return
      }

      store.dispatch({
        type: 'SESSION_STATUS',
        sessionActive: false
      })

      // Calling the end function will send the request
    });
}

export default TwitterLogin;
