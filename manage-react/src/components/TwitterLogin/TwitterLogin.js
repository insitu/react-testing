import React, { Component } from 'react'
// import ReactDOM from 'react-dom';
// Redux store
import store from '../../store'
// Images
import image from './twitterLogo.svg'
// Styles
import './TwitterLogin.css'
import '../../styles/flexboxgrid.min.css'

const superagent = require('superagent')

class TwitterLogin extends Component {

  handleClick(e) {
    e.preventDefault();
    window.location = window.urlAPI + '/oauth_request'
  }

  render() {
    return (

      <div>

        <div className="row center-xs middle-xs">
            <div className="twitter-logo">
              <img src={image} alt="twitter"/>
            </div>
        </div>

        <div className="row center-xs middle-xs">
          <div className="col-lg-2 col-md-3 col-sm-5 col-xs-10 twitter-button" onClick={this.handleClick}>
              Login
          </div>
        </div>

      </div>

    );
  }
}

export default TwitterLogin;
