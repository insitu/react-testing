import React, { Component } from 'react';


// Styles
import './TwitterList.css';
import '../../styles/flexboxgrid.min.css';

const superagent = require('superagent');

class TwitterLogin extends Component {

  constructor(props){
    super(props)
    this.state = {items:[]}
  }

  componentDidMount() {
    this.getTweetItems()
  }

  componentWillUnmount() {
    //
  }

  getTweetItems() {

    superagent
      .get(window.urlAPI + '/tweets')
      .withCredentials()
      .end((err, res) => {
        console.log(res);
        console.log(err);

        this.setState({items: res.body })
        // Calling the end function will send the request
      });
  }

  render() {

    if(!this.state.items){
      return (
        <div className="row center-xs">
          <div className="col-xs-12">
            Session expired..
          </div>
        </div>
      )
    }

    const listItems = this.state.items.map( (item) =>

      <div className="row item">
        <div className="col-xs-12">
          <div className="row end-xs date">
            <div className="col-xs-6">
              {(new Date(item.created_at)).toLocaleString()}
            </div>
          </div>
          <div className="row text">
            <div className="col-xs-12">
              {item.text}
            </div>
          </div>

          <div className="row icons-container">
            <div className="col-xs-2 icon-container">
              <div className="reply-icon icon"></div>
              <div className="icon-label">{item.favorite_count}</div>
            </div>

            <div className="col-xs-2 icon-container">
              <div className="retweet-icon icon"></div>
              <div className="icon-label">{item.retweet_count}</div>
            </div>

            <div className="col-xs-2 icon-container">
              <div className="heart-icon icon"></div>
              <div className="icon-label">{item.heart_count?item.heart_count:0}</div>
            </div>
          </div>

        </div>
      </div>

    );
    return(
      <div className="row center-xs">
        <div className="col-lg-6 col-md-8 col-sm-10 col-xs-11">
          {listItems}
        </div>
      </div>
    );
  }

}
export default TwitterLogin;
