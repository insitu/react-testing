import {createStore} from 'redux'

const reducer = (state, action) => {

  console.log(action);
  console.log(state);


  switch (action.type) {
    case 'SESSION_STATUS':

      return {
        ...state,
        sessionActive: action.sessionActive
      }
      break;

    case 'CHANGE_PROFILE':
      return {
        ...state,
        profile: action.profile
      }
      break;

    default: return state
  }
  return state
}
export default createStore(reducer, {
  sessionActive: false,
  profile: null
})
