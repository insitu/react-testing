import React, { Component } from 'react';
// import { connect } from 'react-redux'
// Redux store
import store from './store'
// Components
import TwitterLogin from './components/TwitterLogin/TwitterLogin.js';
import TwitterRefresh from './components/TwitterRefresh/TwitterRefresh.js';
import TwitterList from './components/TwitterList/TwitterList.js';
// Images
import logo from './images/logo.svg';
// Styles
import './App.css';
import './styles/flexboxgrid.min.css';

const superagent = require('superagent')

class App extends Component {

  constructor(props){
    super(props)

    // Aca deber'ia chequear el store, en caso de que se guarde el estado luego de un refresh
    this.state = {
      sessionActive: false,
      profile: null
    }

    store.subscribe( () => {
      this.setState({
        sessionActive: store.getState().sessionActive
      })
    })
  }

  componentDidMount() {
    let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)KOASESSID\s*\=\s*([^;]*).*$)|^.*$/, "$1");
    if(cookieValue && cookieValue.length > 0) this.getUserData()
  }

  componentWillUnmount() {
    //
  }

  render() {

    return (
      <div className="App">
        <header className="App-header row middle-xs start-xs">
          <div className="col-xs-12">
            <img src={logo} className="App-logo" alt="logo" />
          </div>
        </header>

        <div id="loginContent"></div>

        {(() => {
          if (this.state.sessionActive) {
            return <TwitterRefresh/>
          }else {
            return <TwitterLogin/>
          }
        })()}

        {(() => {
          if (this.state.sessionActive) {
            return <TwitterList/>
          }
        })()}
      </div>
    );
  }

  getUserData(){

        superagent
          .post(window.urlAPI + '/connect')
          .withCredentials()
          .end((err, res) => {

            if(err){
              console.log(err)
              return;
            }
            console.log(res)

            store.dispatch({
              type: 'SESSION_STATUS',
              sessionActive: true
            })

            store.dispatch({
              type: 'CHANGE_PROFILE',
              profile: JSON.parse(res.text)
            })

          });
  }
}

export default App;
