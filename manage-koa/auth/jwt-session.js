const jwt = require('jsonwebtoken')
// const jwt = require('koa-jwt')
const KEY = '3AB720C5-F719-4F32-B13B-149831C936AX'

const EXPIRATION_MINUTES = 120
// const NOEPIRE_DAYS = 14;


/*
* Verify the KOASESSID cookie and renew it, if its valid.
*/
module.exports.isAuthorized = async function(ctx, next) {

  const signedPayload = ctx.cookies.get("KOASESSID")
  const newSignedPayload = _verifyAndRenew(signedPayload)

  // Invalid session terminate
  if (!newSignedPayload) {
    ctx.status = 401
    ctx.body = 'Invalid session or expired'
    // clear cookies
    ctx.cookies.set('KOASESSID', '0', { httpOnly: false })
    return
  }

  // Renew session in cookies
  ctx.cookies.set('KOASESSID', newSignedPayload, { httpOnly: false })
  await next()
}


/*
* Build and return the payload object and return signed
*/
module.exports.getSignedSessionId = (userInfo) => {

  // Validate data object
  if(!userInfo){
    throw new Error('Invalid user to build payload')
  }

  let exp = _getExpirationTimestamp()
  let iat = Math.floor(Number(new Date())/1000)

  userInfo.iat = iat
  userInfo.exp = exp
  return jwt.sign(userInfo, KEY);
}

module.exports.getDecoded = (ctx) => {
  const signedPayload = ctx.cookies.get("KOASESSID")
  return _decodePayload(signedPayload)
}

/******************************************************************************/
// private scope
/******************************************************************************/

/*
* Verify the signedPayload (KOASESSID) is a valid session
* Renew its expiration timestamp and return signed
*/
function _verifyAndRenew(signedPayload){

  // Verify and get decoded object
  let payload = _decodePayload(signedPayload)
  if (!payload) return false

  // renew and hash new expiration timestamp
  payload.exp = _getExpirationTimestamp();
  return jwt.sign(payload, KEY);
}

/*
* Verify the payload and return decoded object or FALSE if invalid
*/
function _decodePayload(signedPayload){

  try {
      return jwt.verify(signedPayload, KEY);
  } catch (e) {
      return false;
  }
}

/*
* Renew the expiration date
*/
function _getExpirationTimestamp(){
  let expDate = new Date();
  expDate = expDate.setMinutes(expDate.getMinutes() + EXPIRATION_MINUTES);

  return Math.floor(Number(expDate)/1000);
}
