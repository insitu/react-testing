const Router = require('koa-router')
const Twitter = require('twitter')
const passport = require('koa-passport')
const jwtSession = require('../auth/jwt-session')

const router = new Router({
  // Normally we would be using '/twitter' to be organized with other controllers.
  prefix: ''
});

/*
* Response with the Twitter authorisation URL
*/
router.get('/oauth_request',
  passport.authenticate('twitter', { session:true, scope: ['include_email=true'] })
)

/*
* Response with all my Twitter profile data - Also passport callback
*/
.post('/connect', jwtSession.isAuthorized, async(ctx, next) => {
  try {
    let userData = jwtSession.getDecoded(ctx)

    // Set only the info we want to show in FE
    delete userData.access_token_key
    delete userData.access_token_secret
    delete userData.exp
    delete userData.iat

    ctx.body = JSON.stringify(userData)

  } catch (e) {
    ctx.status = 401
    ctx.body = 'No authorized'
  }
})

/*
* Response with a list containing 100 of my most recent tweets
*/
.get('/tweets', jwtSession.isAuthorized, async (ctx, next) => {
  try {
    const userData = jwtSession.getDecoded(ctx)
    ctx.body = await getRecentTweets(userData)
  } catch (e) {
    ctx.status = 401
    ctx.body = 'No authorized'
  }
})

/*
* Disconnected from Twitter and receive a response with my TwitterID for confirmation
*/
.post('/disconnect', async (ctx, next) => {
  ctx.cookies.set('KOASESSID', '0', { httpOnly: false })
  ctx.body = {data:'Done'}
})

/*
* Passport callback
*/
.get('/passport-callback', passport.authenticate('twitter', {session:true, failureRedirect: '/' }),
  (ctx, next) => {

    //Twitter login failed, logout.
    if(!ctx.isAuthenticated()){
        ctx.cookies.set('KOASESSID', '0', { httpOnly: false });
        ctx.status = 401;
        ctx.redirect(process.env.frontend)
        return;
    }

    // Info to be enconded in the token
    let signedInfo = {
      id: ctx.req.user.id,
      name: ctx.req.user.name,
      screen_name: ctx.req.user.screen_name,
      description: ctx.req.user.description,
      email: ctx.req.user.email,
      profile_image_url: ctx.req.user.profile_image_url,
      followers_count: ctx.req.user.followers_count,
      friends_count: ctx.req.user.friends_count,
      listed_count: ctx.req.user.listed_count,
      created_at: ctx.req.user.created_at,
      access_token_key: ctx.req.user.access_token_key,
      access_token_secret: ctx.req.user.access_token_secret
    }
    // Set cookie and redirect to FE app
    const koaSessId = jwtSession.getSignedSessionId(signedInfo)
    ctx.cookies.set('KOASESSID', koaSessId, { httpOnly: false })
    ctx.redirect(process.env.frontend)
  }
)


module.exports = router;

async function getRecentTweets(userData){

  // Create Twitter client object to validate
  const client = new Twitter({
    consumer_key: process.env.TWITTER_CONSUMER_KEY,
    consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
    access_token_key: userData.access_token_key, // process.env.TWITTER_ACCESS_TOKEN_KEY,
    access_token_secret: userData.access_token_secret // process.env.TWITTER_ACCESS_TOKEN_SECRET
  })

  return new Promise( (resolve, reject) => {
    try {
      // call twitter API
      const options = {user_id:userData.id, count:100}
      client.get('statuses/user_timeline', options, (error, tweets, response) => {
        if(error) reject('error')
        resolve(tweets)
      });
    } catch (err) {
      reject('error')
    }
  })
}
