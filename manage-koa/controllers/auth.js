const passport = require('koa-passport')

process.env.TWITTER_CONSUMER_KEY = 'NGPEp16WKvHTKZCv94xmvy1iw'
process.env.TWITTER_CONSUMER_SECRET = 'h8YCl9AOL2yRDEKM10ZXwlAQBtMwjCLRnXTWy96VIqhAXxZYgk'

const fetchUser = (() => {
  // This would be the place to find the user in database
  const user = { id: 1, username: 'manageSocial' }
  return async function() {
    return user
  }
})()

passport.serializeUser(function(user, done) {
  done(null, user)
})

passport.deserializeUser(async function(id, done) {
  try {
    const user = await fetchUser()
    done(null, user)
  } catch(err) {
    done(err)
  }
})

const TwitterStrategy = require('passport-twitter').Strategy
passport.use(new TwitterStrategy({
    includeEmail: true,
    consumerKey: process.env.TWITTER_CONSUMER_KEY,
    consumerSecret: process.env.TWITTER_CONSUMER_SECRET,
    callbackURL: 'http://'+ process.env.hostname + ':' + process.env.PORT + '/passport-callback'
  },
  function(token, tokenSecret, profile, done) {

    // Retrieve user information
    let twitterUser = profile._json
    delete twitterUser.status
    twitterUser.access_token_key = token
    twitterUser.access_token_secret = tokenSecret

    // retrieve user
    // fetchUser().then(user => done(null, user))
    done(null, twitterUser)
  }
))
