const Koa = require('koa')
const Router = require('koa-router')
const twitter = require('./controllers/twitter')

const app = new Koa()

// environment
process.env.PORT = 3000;
process.env.hostname = 'localhost'
process.env.frontend = 'http://localhost:3001/' // used for passport-twitter callback

// CORS
// const cors = require('@koa/cors');
const cors = require('kcors')
var corsOptions = {credentials: true} // this is to receive the JWT in cookies
app.use(cors(corsOptions))


// sessions
const session = require('koa-session')
app.keys = ['your-session-secret']
app.use(session({}, app))

// authentication
require('./controllers/auth')
const passport = require('koa-passport')
app.use(passport.initialize())
app.use(passport.session())

// router = new Router()
// router.get("/", async ctx => {
//   ctx.body = {
//     data: "Done"
//   }
// })
// app.use(router.routes());

// routes
app.use(twitter.routes())
   .use(twitter.allowedMethods())

app.on('error', (err, ctx) => {
  console.log('*** ERROR HANDLER ***')
  console.log(err)
})


module.exports = app.listen(process.env.PORT, process.env.hostname, () => {
  console.log(`Hey there! I am running at http://${process.env.hostname}:${process.env.PORT}/`)
})
