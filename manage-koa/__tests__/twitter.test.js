// require the Koa server
const app = require("../index.js");
const request = require("supertest");

// close the server after each test
afterEach(() => {
  app.close();
})

describe("routes: index", () => {
  test("should respond as expected", async () => {
    const response = await request(app).get("/")
    expect(response.status).toEqual(404)
  })
})

describe("routes: disconnect", () => {
  test("should respond as expected", async () => {
    const response = await request(app).post("/disconnect")
    expect(response.status).toEqual(200)
    expect(response.type).toEqual("application/json")
    expect(response.body.data).toEqual("Done")
  })
})

describe("route tweets", () => {
  test("should restrict access", async () => {
    const response = await request(app).get("/tweets")
    expect(response.status).toEqual(401)
  })
})
