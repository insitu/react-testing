# React - Redux - Koa.js and Twitter API #

### What is this repository for? ###

This is a small project to apply and learn these technologies, from top to bottom:
* React
* Redux
* Redux-Saga
* Koa.js
* Passport
* JWT (tokens for stateless session)
* Twitter API

Version 1.0

## How do I get set up? ##

### 1. Clone and run the projects ###

Just follow the next steps.

1. Open a console and clone the project in a testing folder:
> git clone https://bitbucket.org/insitu/356a4d26706c53f414cb3d87600b8e73.git

2. Open the koa project
> cd 356a4d26706c53f414cb3d87600b8e73
> cd manage-koa
> npm install
> npm start

3. The next message should be shown in console:
> Hey there! I am running at http://localhost:3000/

4. Open a new console go to React project, install dependencies and start project
> cd 356a4d26706c53f414cb3d87600b8e73/manage-react
> npm install
> npm start

5. Since port 3000 is already used, you will be prompted to use 3001 or other.
> Choose yes


##### Dependencies #####
 * Node >= 6


### 2. Verify oAuth callback is pointing to React's url ###
During the login process using Twitter API, we need specify a callback url for the Twitter API to return,
after a success login.

A normal scenario would be that you have Koa running on port 3000 and React on port 3000. If this is the case,
you can skip this section.

It is not the case ? Then continue reading.

You will find two folders in the repository. One for the BE (manage-koa) ad other for FE (manage-react).
Go to manage-koa and edit the file index.js. Change the line:

> process.env.frontend = "http://localhost:3001/"

For something that is pointing to you front end. That's it.


## Running tests ##
Tests are located in __tests__ folder. Unit tests are written using jest.

Simply go to the project folder and run
> npm test

.

#### Repo owner ####

> Damian Buzzaqui
